// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'rocksmithleaderboards',
    appId: '1:84446026843:web:a471243ef60d433df9b37e',
    databaseURL: 'https://rocksmithleaderboards-default-rtdb.firebaseio.com',//'localhost:9000?ns=rocksmith-leaderboards',
    storageBucket: 'rocksmithleaderboards.appspot.com',
    apiKey: 'AIzaSyDIEXKpGaFanH4wvtL8JbmbPtBj_WdwXbE',
    authDomain: 'rocksmithleaderboards.firebaseapp.com',//'localhost:9099?ns=rocksmith-leaderboards',
    messagingSenderId: '84446026843',
    measurementId: 'G-Z5RNMY9XN9',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
