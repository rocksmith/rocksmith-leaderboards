import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { collection, endAt, getDocs, orderBy, query, startAt } from 'firebase/firestore';

export interface SongData {
  id: string;
  artist: string;
  title: string;
  album: string;
}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements AfterViewInit {
  dataSource: MatTableDataSource<SongData>;
  displayedColumns = ['link', 'artist', 'title', 'album'];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private db: Firestore) {
    this.dataSource = new MatTableDataSource();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  async search(searchString: string) {
    let searchResults: SongData[] = [];

    const artistMatches = await this.searchSongData('artist', searchString);
    const titleMatches = await this.searchSongData('title', searchString);
    const albumMatches = await this.searchSongData('album', searchString);

    searchResults = [...artistMatches, ...titleMatches, ...albumMatches];

    this.dataSource.data = searchResults;
  }

  private async searchSongData(field: string, searchString: string): Promise<SongData[]> {
    let searchResults: SongData[] = [];

    const songsRef = collection(this.db, 'songs');

    const q = query(songsRef, orderBy(field), startAt(searchString), endAt(searchString + '\uf8ff'));
    const snapshot = await getDocs(q);
    snapshot.forEach((doc) => {
      const data = doc.data();

      const songData: SongData = {
        id: doc.id,
        album: data['album'],
        artist: data['artist'],
        title: data['title'],
      };

      searchResults.push(songData);
    });

    return searchResults;
  }
}
