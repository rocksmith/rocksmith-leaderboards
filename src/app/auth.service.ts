import { Injectable } from '@angular/core';
import { Auth, connectAuthEmulator, onAuthStateChanged, signInWithEmailAndPassword } from '@angular/fire/auth';
import { Database, ref, update } from '@angular/fire/database';
import { Router } from '@angular/router';
import { signOut } from 'firebase/auth';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userData: any;

  constructor(private auth: Auth, private database: Database, private router: Router) {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user')!);
      } else {
        localStorage.setItem('user', 'null');
        JSON.parse(localStorage.getItem('user')!);
      }
    });
  }

  signIn(email: string, password: string) {
    signInWithEmailAndPassword(this.auth, email, password)
      .then((userCredential) => {
        this.setUserData(userCredential.user);

        onAuthStateChanged(this.auth, (user) => {
          if (user) {
            this.router.navigate(['dashboard']);
          }
        });
      })
      .catch((error) => {
        alert(error.message);
      });
  }

  setUserData(user: any) {
    const userData: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
    };

    return update(ref(this.database, 'users/' + user.uid), {
      userData
    });
  }
  
  signOut() {
    return signOut(this.auth)
      .then(() => {
        localStorage.removeItem('user');
        this.router.navigate(['sign-in']);
      });
  }
}
