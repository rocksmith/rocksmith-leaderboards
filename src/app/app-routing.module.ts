import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Custom components
import { LeaderboardsComponent } from './leaderboards/leaderboards.component';
import { ScoreImportComponent } from './score-import/score-import.component';
import { SearchComponent } from './search/search.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';

const routes: Routes = [
  { path: '', redirectTo: '/search', pathMatch: 'full' },
  { path: 'search', component: SearchComponent },
  { path: 'sign-in', component: SignInComponent },
 // { path: 'sign-up', component: SignUpComponent },
  { path: 'leaderboards/:id', component: LeaderboardsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
