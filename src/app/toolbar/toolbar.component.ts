import { Component } from '@angular/core';
import { SignInComponent } from '../sign-in/sign-in.component';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent {
  signInOpen = false;

  constructor() { }

  openSignIn() {
    this.signInOpen = true;
  }
}
