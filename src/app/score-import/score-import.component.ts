import { Component } from '@angular/core';
import { Database ,ref, set, update } from "@angular/fire/database";
import { Md5 } from 'ts-md5/dist/md5';

@Component({
  selector: 'app-score-import',
  templateUrl: './score-import.component.html',
  styleUrls: ['./score-import.component.scss']
})
export class ScoreImportComponent {
  file: any;
  submitDisabled = true;

  constructor(private database: Database) { }

  fileChanged(event: any) {
    this.file = event.target.files[0];
    this.submitDisabled = false;
  }

  submit() {
    let fileReader = new FileReader();
    fileReader.onload = () => {
      let data = JSON.parse(fileReader.result as string);
      let songsData = data['songs'];

      let hash = Md5.hashStr(JSON.stringify(songsData));

      if (hash !== data["hash"]) {
        alert("ALERT: CHEATER DETECTED!\nDo not try to modify your scores or you will be banned.\nConsider this your last warning.");
        //TODO: log that someone tried to cheat
        return;
      }

      // Extract song data
      let updates: any = {};
      for (let songKey in songsData) {
        let songPath = '/songs/' + songKey;

        // Update song data
        updates[songPath + '/album'] = songsData[songKey]['album'];
        updates[songPath + '/albumSort'] = songsData[songKey]['albumSort'];
        updates[songPath + '/artist'] = songsData[songKey]['artist'];
        updates[songPath + '/artistSort'] = songsData[songKey]['artistSort'];
        updates[songPath + '/title'] = songsData[songKey]['title'];
        updates[songPath + '/titleSort'] = songsData[songKey]['titleSort'];
        updates[songPath + '/year'] = songsData[songKey]['year'];

        let arrangementsData = songsData[songKey]['arrangements']
        for (let arrangementKey in arrangementsData) {
          let arrangementPath = '/arrangements/' + songKey + '/' + arrangementKey;

          // Update list of arrangements 
          updates[arrangementPath + '/name'] = arrangementsData[arrangementKey]['name'];

          let scoresData = arrangementsData[arrangementKey]['scores'];
          for (let difficulty in scoresData) {
            let scoresPath = '/scores/' + songKey + '/' + arrangementKey + '/' + difficulty;

            // Update scores for each difficulty
            updates[scoresPath + '/' + 'TNTMusicStudios' + '/score'] = scoresData[difficulty];
          }
        }
      }

      update(ref(this.database), updates);
      console.log("Value set?");
    }

    fileReader.readAsText(this.file);
  }
}
