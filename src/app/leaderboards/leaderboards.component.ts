import { Component } from '@angular/core';

@Component({
  selector: 'app-leaderboards',
  templateUrl: './leaderboards.component.html',
  styleUrls: ['./leaderboards.component.scss']
})
export class LeaderboardsComponent {
  artist = "Artist 1";
  song = "Song 1";
  path = "Lead";
  difficulty = "Hard";
  entries = [
    {
      rank: 1,
      user: "TNTMusicStudios",
      score: 1000000,
    },
    {
      rank: 2,
      user: "StajiW",
      score: 999999,
    },
    {
      rank: 3,
      user: "IronDeuce",
      score: 69420,
    },
    {
      rank: 4,
      user: "SirByTheDoor",
      score: "bad",
    },
  ];
  readonly displayedColumns = ['rank', 'user', 'score'];

  constructor() { }

}
